// Mi primer JavaScript
console.log("Hello World");

let firstName = "Talo";
console.log(firstName);

const interesRate = 0.3;
//interesRate = 1;
console.log(interesRate);

//-----------------------------------------------//
//---------------- PRIMITIVE - VALUE  ----------------//
//-----------------------------------------------//
let nombre = "Talo"; // String
let edad = 31; // Number (All numbers are number)
let Casado = false; // Boolean
let fechaNacimiento = undefined; // undefined
let equipo = null; // null

//DINAMIC - VALUE

//-----------------------------------------------//
//------------------- OBJETOS -------------------//
//-----------------------------------------------//

let person = {
  name: "talo",
  age: 31,
};

person.name = "Italo";

let selection = "name";
person[selection] = "Taloski";

console.log(person.name);

//-----------------------------------------------//
//-------------------- ARRAYS -------------------//
//-----------------------------------------------//

let selectColors = ["red", "blue"];

selectColors[2] = 1;
selectColors[3] = null;

console.log(selectColors.length);

//-----------------------------------------------//
//------------------ FUNCTIONS ------------------//
//-----------------------------------------------//

//Performing a Task
function saludo(name) {
  console.log("Hello " + name);
}

let name = "Juanito";

saludo(name);

//Calculating a value
function square(number) {
  return number * number;
}

console.log(square(5));
